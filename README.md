**Modified the framework to add few components and changed the control method from slider to touch.** - Karan Kumar

# Doodlejump Framework

Doodlejump is a simple and fun game.  You have a guy jumping on platforms, going up.  That's pretty much it.

If you miss platforms, you hit the ground and lose.  The goal it to jump as high as you can.
With the framework, if you jump past the top, you wrap around to the bottom, and if you hit the bottom,
you just bounce up again.  Plenty of room to customize things so that platforms disappear or move,
there are barriers you need to avoid, and power-ups of different kinds.

The physics simulation is simple; it should be easy to modify.

iOS has some APIs that make this sort of animation even easier -- but for this one, we're doing it kind
of "old school" to show what sorts of things are under the hood of the APIs.