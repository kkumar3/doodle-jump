

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"back1.jpg"]];
    
    UILabel *scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(300, 50, 50, 50)];
    [scoreLabel setBackgroundColor:[UIColor clearColor]];
    [scoreLabel setText:@"Score"];
    [[self view] addSubview:scoreLabel];
    
        
   
    
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];		}

@end
