
#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView {

}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
-(void)arrange:(CADisplayLink *)sender;
@property (nonatomic, strong) UILabel *score;
@property (nonatomic) int scoreCount;

@end
